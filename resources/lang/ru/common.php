<?php return  [
  'meta_title' => '',
  'meta_description' => '',

  'main' => 'Главная',
  'about' => 'Об ',
  'contacts' => 'Контакты',
  'why' => 'Почему',
  'rent' => 'Аренда',
  'sale' => 'Продажа',
  'team' => 'Команда',
  'recommended' => 'рекомендованные',
  'objects' => 'обьекты',
  'categoryes' => 'категорию',
  'choose' => 'выберите',
  'agents' => 'Агенты',
  'our' => 'наши',
  'navigation' => 'Навигация',

  'about_company' => 'О нас',
  'about_title' => 'Выбрали именно нас',
  'about_subtitle' => 'Мы предлагаем безупречные услуги в сфере недвижимости',
  'team_about' => 'Мы предоставляем полный сервис на каждом этапе.',
  'team_more' => 'Смотреть всех',
  '404_title' => 'Страница не найдена!',
  '404_text' => 'Ой! Похоже, что-то идет не так Мы не можем найти страницу, которую вы ищете, убедитесь, что вы ввели правильный URL-адрес',

  'main_office_phone_value_1' => '+456875369208',
  'main_office_phone_label_1' => '+456 875 369 208',
  'main_office_address' => '95 South Park Avenue, USA',
  'main_office_mail' => 'support@findhouses.com',
  'copyright' => '2021 © Copyright - All Rights Reserved.'
];
