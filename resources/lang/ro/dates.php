<?php

return [
    'january' => 'ianuarie',
    'february' => 'februarie',
    'march' => 'martie',
    'april' => 'aprilie',
    'may' => 'mai',
    'june' => 'iunie',
    'july' => 'iulie',
    'august' => 'august',
    'september' => 'septembrie',
    'october' => 'octombrie',
    'november' => 'noiembrie',
    'december' => 'decembrie',
];
