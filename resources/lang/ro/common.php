<?php return  [
  'meta_title' => '',
  'meta_description' => '',

  'main' => 'Pagina principală',
  'about' => 'Despre',
  'contacts' => 'Contacte',
  'why' => 'De ce',
  'rent' => 'Arenda',
  'sale' => 'Vânzare',
  'team' => 'Echipa',
  'recommended' => 'recomandate',
  'objects' => 'obiecte',
  'categoryes' => 'categoria',
  'choose' => 'alege',
  'agents' => 'agenții',
  'our' => 'noștri',
  'navigation' => 'Navigare',

  'about_company' => 'Despre noi',
  'about_title' => 'Ne-ai ales',
  'about_subtitle' => 'Oferim servicii imobiliare impecabile',
  'team_about' => 'Oferim servicii complete la fiecare pas.',
  'team_more' => 'Vezi pe toţi',
  '404_title' => 'Pagina nu a fost gasita!',
  '404_text' => 'Hopa! Se pare că nu putem găsi pagina pe care o căutați, asigurați-vă că ați introdus adresa URL actuală',

  'main_office_phone_value_1' => '+456875369208',
  'main_office_phone_label_1' => '+456 875 369 208',
  'main_office_address' => '95 South Park Avenue, USA',
  'main_office_mail' => 'support@findhouses.com',
  'copyright' => '2021 © Copyright - All Rights Reserved.'
];
