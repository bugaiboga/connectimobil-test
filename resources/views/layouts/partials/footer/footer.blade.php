        <!-- START FOOTER -->
        <footer class="first-footer">
            <div class="top-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-5 col-lg col-xl">
                            <div class="netabout">
                                <a href="{{route('main.index')}}" class="logo">
                                    <img src="images/logo-footer.svg" alt="netcom">
                                </a>
                                <p>2021 © Copyright - All Rights Reserved.</p>
                            </div>
                        </div>
                        <div class="col-sm-2 ml-auto">
                            @include('layouts.partials.footer.footer-menu')
                        </div>
                        <div class="col-sm-3 ml-auto">
                            <div class="contactus">
                                <ul>
                                    <li>
                                        <div class="info">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <p class="in-p">{{ __('common.main_office_address') }}</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="info">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <p class="in-p">{{ __('common.main_office_phone_label_1') }}</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="info">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <p class="in-p ti">{{__('common.main_office_mail')}}</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <a data-scroll href="#wrapper" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
        <!-- END FOOTER -->