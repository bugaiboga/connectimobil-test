<div class="navigation">
    <h3>{{__('common.navigation')}}</h3>
    <div class="nav-footer">
        <ul>
            <li><a href="#">{{ __('common.main') }}</a></li>
            <li><a href="#">{{ __('common.rent') }}</a></li>
            <li><a href="#">{{ __('common.sale') }}</a></li>
        </ul>
        <ul class="nav-right">
            <li><a href="#">{{__('common.about_company')}}</a></li>
            <li><a href="#">{{__('common.team')}}</a></li>
            <li><a href="contact-us.html">{{__('common.contacts')}}</a></li>
        </ul>
    </div>
</div>