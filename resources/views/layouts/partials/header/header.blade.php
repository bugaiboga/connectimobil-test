<header id="header-container">
    <!-- Header -->
    <div id="header">
        <div class="container container-header">
            <!-- Left Side Content -->
            <div class="left-side">
                <!-- Logo -->
                <div id="logo">
                    <a href="{{ route('main.index') }}"><img src="images/logo-red.svg" alt=""></a>
                </div>
                <!-- Mobile Navigation -->
                <div class="mmenu-trigger">
                    <button class="hamburger hamburger--collapse" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
                @include('layouts.partials.header.header-menu')
                <!-- Left Side Content / End -->
            </div>
            <!-- lang-wrap-->
            <div class="header-user-menu user-menu add d-none d-lg-none d-xl-flex">
                @include('layouts.partials.block.lang-switcher')
            </div>
            <!-- lang-wrap end-->
        </div>
        <!-- Header / End -->

</header>
<div class="clearfix"></div>