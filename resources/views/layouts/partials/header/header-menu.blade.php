  <!-- Main Navigation -->
  <nav id="navigation" class="style-1">
      <ul id="responsive">
          <li>
              <a href="{{ route('main.index') }}">{{__('common.main') }}</a>
          </li>
          <li><a href="#">{{__('common.rent') }}</a>
              <ul>
                  <li><a href="#">{{ __('common.rent') }} Grid</a>
                      <ul>
                          <li><a href="properties-grid-1.html">Grid View 1</a></li>
                          <li><a href="properties-grid-2.html">Grid View 2</a></li>
                          <li><a href="properties-grid-3.html">Grid View 3</a></li>
                          <li><a href="properties-grid-4.html">Grid View 4</a></li>
                          <li><a href="properties-full-grid-1.html">Grid Fullwidth 1</a></li>
                          <li><a href="properties-full-grid-2.html">Grid Fullwidth 2</a></li>
                          <li><a href="properties-full-grid-3.html">Grid Fullwidth 3</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Listing List</a>
                      <ul>
                          <li><a href="properties-full-list-1.html">List View 1</a></li>
                          <li><a href="properties-list-1.html">List View 2</a></li>
                          <li><a href="properties-full-list-2.html">List View 3</a></li>
                          <li><a href="properties-list-2.html">List View 4</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Listing Map</a>
                      <ul>
                          <li><a href="properties-half-map-1.html">Half Map 1</a></li>
                          <li><a href="properties-half-map-2.html">Half Map 2</a></li>
                          <li><a href="properties-half-map-3.html">Half Map 3</a></li>
                          <li><a href="properties-top-map-1.html">Top Map 1</a></li>
                          <li><a href="properties-top-map-2.html">Top Map 2</a></li>
                          <li><a href="properties-top-map-3.html">Top Map 3</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Agent View</a>
                      <ul>
                          <li><a href="agents-listing-grid.html">Agent View 1</a></li>
                          <li><a href="agents-listing-row.html">Agent View 2</a></li>
                          <li><a href="agents-listing-row-2.html">Agent View 3</a></li>
                          <li><a href="agent-details.html">Agent Details</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Agencies View</a>
                      <ul>
                          <li><a href="agencies-listing-1.html">Agencies View 1</a></li>
                          <li><a href="agencies-listing-2.html">Agencies View 2</a></li>
                          <li><a href="agencies-details.html">Agencies Details</a></li>
                      </ul>
                  </li>
              </ul>
          </li>
          <li><a href="#">{{__('common.sale') }}</a>
              <ul>
                  <li><a href="single-property-1.html">Single Property 1</a></li>
                  <li><a href="single-property-2.html">Single Property 2</a></li>
                  <li><a href="single-property-3.html">Single Property 3</a></li>
                  <li><a href="single-property-4.html">Single Property 4</a></li>
                  <li><a href="single-property-5.html">Single Property 5</a></li>
                  <li><a href="single-property-6.html">Single Property 6</a></li>
              </ul>
          </li>
          <li>
              <a href="{{ route('about.index') }}">{{__('common.about_company')}}</a>
          </li>
          <li>
              <a href="#">{{__('common.team')}}</a>
          </li>
          <li>
              <a href="contact-us.html">{{__('common.contacts')}}</a>
          </li>
      </ul>
  </nav>
  <!-- Main Navigation / End -->