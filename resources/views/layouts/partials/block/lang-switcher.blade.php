<!-- lang-wrap-->
@if(app()->getLocale() == 'ru')
{{--Russian--}}
<div class="lang-wrap">
    <div class="show-lang"><span><i class="fas fa-globe-americas"></i><strong>РУС</strong></span><i class="fa fa-caret-down arrlan"></i></div>
    <ul class="lang-tooltip lang-action no-list-style">
        <li><a href="{{ route('locale', ['locale' => 'ru']) }}" class=" current-lan" data-lantext="Ru">Русский</a></li>
        <li><a href="{{ route('locale', ['locale' => 'ro']) }}" data-lantext=" Ro">Румынский</a></li>
    </ul>
</div>
@else
{{--Romana--}}
<div class="lang-wrap">
    <div class="show-lang"><span><i class="fas fa-globe-americas"></i><strong>RO</strong></span><i class="fa fa-caret-down arrlan"></i></div>
    <ul class="lang-tooltip lang-action no-list-style">
        <li><a href="{{ route('locale', ['locale' => 'ro']) }}" class="current-lan" data-lantext=" Ro">Română</a></li>
        <li><a href="{{ route('locale', ['locale' => 'ru']) }}" data-lantext="Ru">Rusă</a></li>
    </ul>
</div>
@endif

<!-- lang-wrap end-->