<nav class="breadcrumbs page__breadcrumbs d-block d-md-inline-block d-sm-none">
    <ul class="breadcrumbs__list color-grey2 d-flex">
        <li>
            <a href="{{ route('index') }}" class="fs-12 color-grey2">
                @lang('common.main_page')
            </a>
            &nbsp; › &nbsp;
        </li>
        @foreach ($breadcrumbs as $item)
            <li>
                <a href="{{ $item['url'] ?? '#' }}" class="fs-12 color-blue">{{ $item['name'] }}</a>
                @if (!$loop->last)&nbsp; › &nbsp;@endif
            </li>
        @endforeach
    </ul>
</nav>
