@extends('layouts.about-layout')

@section('content')
<section class="headings">
    <div class="text-heading text-center">
        <div class="container">
            <h1>{{__('common.about_company')}}</h1>
            <h2><a href="index.html">Home </a> &nbsp;/&nbsp; About Us</h2>
        </div>
    </div>
</section>
<!-- END SECTION HEADINGS -->

<!-- START SECTION ABOUT -->
<section class="about-us fh">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">{{__('common.about')}} <span>Find Houses</span></h2>
                </div>
                <div class="pftext">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.</p>
                </div>
                <div class="box bg-2">
                    <a href="about.html" class="text-center button button--moema button--text-thick button--text-upper button--size-s">read More</a>
                    <img src="images/signature.png" class="ml-5" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="wprt-image-video w50">
                    <img alt="image" src="images/bg/bg-video.jpg">
                    <a class="icon-wrap popup-video popup-youtube" href="https://www.youtube.com/watch?v=2xHQqYRcrx4">
                        <i class="fa fa-play"></i>
                    </a>
                    <div class="iq-waves">
                        <div class="waves wave-1"></div>
                        <div class="waves wave-2"></div>
                        <div class="waves wave-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION ABOUT -->

<!-- START SECTION WHY CHOOSE US -->
@include('layouts.partials.block.about-side')
<!-- END SECTION WHY CHOOSE US -->

<!-- START SECTION AGENTS -->
<section class="team fh">
    <div class="container">
        <div class="sec-title">
            <h2>
                @if(app()->getLocale() == 'ru')
                {{--Russian--}}
                @else
                <span>{{ __('common.agents') }} </span> {{ __('common.our') }}
                @endif
            </h2>
            <p>{{ __('common.team_about') }}</p>
        </div>
        <div class="row team-all">
            <div class="col-lg-3 col-md-6 team-pro">
                <div class="team-wrap">
                    <div class="team-img">
                        <img src="images/team/t-5.jpg" alt="" />
                    </div>
                    <div class="team-content">
                        <div class="team-info">
                            <h3>Carls Jhons</h3>
                            <p>Financial Advisor</p>
                            <div class="team-socials">
                                <ul>
                                    <li>
                                        <a href="#" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#" title="instagran"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <span><a href="team-details.html">View Profile</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 team-pro">
                <div class="team-wrap">
                    <div class="team-img">
                        <img src="images/team/t-6.jpg" alt="" />
                    </div>
                    <div class="team-content">
                        <div class="team-info">
                            <h3>Arling Tracy</h3>
                            <p>Acountant</p>
                            <div class="team-socials">
                                <ul>
                                    <li>
                                        <a href="#" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#" title="instagran"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <span><a href="team-details.html">View Profile</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 team-pro pb-none">
                <div class="team-wrap">
                    <div class="team-img">
                        <img src="images/team/t-7.jpg" alt="" />
                    </div>
                    <div class="team-content">
                        <div class="team-info">
                            <h3>Mark Web</h3>
                            <p>Founder &amp; CEO</p>
                            <div class="team-socials">
                                <ul>
                                    <li>
                                        <a href="#" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#" title="instagran"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <span><a href="team-details.html">View Profile</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 team-pro kat">
                <div class="team-wrap">
                    <div class="team-img">
                        <img src="images/team/t-8.jpg" alt="" />
                    </div>
                    <div class="team-content">
                        <div class="team-info">
                            <h3>Katy Grace</h3>
                            <p>Team Leader</p>
                            <div class="team-socials">
                                <ul>
                                    <li>
                                        <a href="#" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#" title="instagran"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <span><a href="team-details.html">View Profile</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-all">
            <a href="about.html" class="btn btn-outline-light">{{__('common.team_more')}}</a>
        </div>
    </div>
</section>
<!-- END SECTION AGENTS -->
@endsection